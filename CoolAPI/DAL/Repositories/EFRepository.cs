﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class EFRepository<T> : IGenericRepository<T> where T : Entity
    {
        private readonly ProjectDbContext _context;
        private readonly DbSet<T> _set;

        public bool Any() => _set.Any();

        public EFRepository(ProjectDbContext context)
        {
            _context = context;
            _set = _context.Set<T>();
        }
        public void Delete(int id)
        {
            var entity = _set.Find(id) ?? throw new KeyNotFoundException();
            _set.Remove(entity);
            _context.SaveChanges();
        }

        public IEnumerable<T> GetAll()
        {
            return _set.ToList();
        }

        public T GetById(int id)
        {
            return _set.Find(id) ?? throw new KeyNotFoundException();
        }

        public void Insert(T obj)
        {
            if (obj is null) throw new ArgumentNullException(nameof(obj));
            if (_set.Any(i => i.Id == obj.Id)) throw new ArgumentException("Error, such Id already exists", nameof(obj));
            _set.Add(obj);
            _context.SaveChanges();
        }

        public void Update(T obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            var entity = _set.Find(obj.Id) ?? throw new KeyNotFoundException();

            _context.Entry(entity).CurrentValues.SetValues(obj);
            _context.SaveChanges();

        }
    }
}
