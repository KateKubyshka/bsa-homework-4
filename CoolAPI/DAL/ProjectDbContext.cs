﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options):base(options)
        {

        }
        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ProjectTask> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureIds(modelBuilder);

            ConfigureRelations(modelBuilder);

            ConfigureProjectProperties(modelBuilder);

            ConfigureProjectTaskProperties(modelBuilder);

            ConfigureTeamProperties(modelBuilder);

            ConfigureUserProperties(modelBuilder);

            ConfigureIndexes(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private static void ConfigureIndexes(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().HasIndex(p => p.Name);
            modelBuilder.Entity<ProjectTask>().HasIndex(t => t.Name);
            modelBuilder.Entity<Team>().HasIndex(t => t.Name);
            modelBuilder.Entity<User>().HasIndex(u => new { u.FirstName, u.LastName });
            modelBuilder.Entity<User>().HasIndex(u => u.Email).IsUnique();
        }

        private static void ConfigureUserProperties(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                        .Property(u => u.FirstName)
                        .IsRequired()
                        .HasMaxLength(200);
            modelBuilder.Entity<User>()
                        .Property(u => u.Email)
                        .HasMaxLength(100);
            modelBuilder.Entity<User>()
                        .Property(u => u.RegisteredAt)
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<User>()
                        .Property(u => u.LastName)
                        .HasMaxLength(100);
        }

        private static void ConfigureTeamProperties(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>()
                        .Property(t => t.Name)
                        .IsRequired();
            modelBuilder.Entity<Team>()
                        .Property(t => t.CreatedAt)
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GETDATE()");
        }

        private static void ConfigureProjectTaskProperties(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProjectTask>()
                        .Property(p => p.Name)
                        .IsRequired()
                        .HasMaxLength(200);

            modelBuilder.Entity<ProjectTask>()
                        .Property(t => t.State)
                        .IsRequired();

            modelBuilder.Entity<ProjectTask>()
                        .Property(t => t.Description)
                        .HasMaxLength(500);

            modelBuilder.Entity<ProjectTask>()
                        .Property(t => t.CreatedAt)
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GETDATE()");
        }

        private static void ConfigureProjectProperties(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>()
                        .Property(p => p.CreatedAt)
                        .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Project>()
                        .Property(p => p.Description)
                        .HasMaxLength(500);

            modelBuilder.Entity<Project>()
                        .Property(p => p.Name)
                        .IsRequired()
                        .HasMaxLength(200);
        }

        private static void ConfigureIds(ModelBuilder modelBuilder) 
        {
            ConfigureIdOne<Project>(modelBuilder);
            ConfigureIdOne<ProjectTask>(modelBuilder);
            ConfigureIdOne<Team>(modelBuilder);
            ConfigureIdOne<User>(modelBuilder);
        }

        private static void ConfigureIdOne<T>(ModelBuilder modelBuilder) where T : Entity
        {
            modelBuilder.Entity<T>()
                        .HasKey(x => x.Id);

            modelBuilder.Entity<T>()
                        .Property(x => x.Id)
                        .ValueGeneratedNever();
        }

        private static void ConfigureRelations(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>() // one project to many tasks
                        .HasMany(p => p.Tasks)
                        .WithOne(t => t.Project)
                        .HasForeignKey(t => t.ProjectId)
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Team>() // one team to many users 
                        .HasMany(t => t.Users)
                        .WithOne(u => u.Team)
                        .HasForeignKey(u => u.TeamId)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Team>() // one team to many projects
                        .HasMany(t => t.Projects)
                        .WithOne(p => p.Team)
                        .HasForeignKey(p => p.TeamId)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>() // one user to many projects
                        .HasMany(u => u.Projects)
                        .WithOne(p => p.Author)
                        .HasForeignKey(p => p.AuthorId)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>() // one user to many tasks
                        .HasMany(u => u.Tasks)
                        .WithOne(t => t.Performer)
                        .HasForeignKey(t => t.PerformerId)
                        .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
